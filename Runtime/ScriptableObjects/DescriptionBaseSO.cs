namespace GameLibrary.StateMachines.ScriptableObjects
{
    using UnityEngine;

    /// <summary>
    /// Base class for StateMachine ScriptableObjects that need a description field.
    /// </summary>
    public abstract class DescriptionBaseSO : ScriptableObject
    {
        [TextArea]
        public string description;
    }
}
