# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [1.2.1] - 2022-09-05
### Fixed
- Fixed template scripts


## [1.2.0] - 2022-03-18
### Fixed
- Remove unused using directives


## [1.1.1] - 2021-10-28
### Fix
- Licensing stuff


## [1.0.1] - 2021-10-28
### Changed
- Make Bool & GameObject channels inherit from BaseSO.
- StateMachine: Change transition editor menu title.

### Fixed
- Actions and Conditions creation using template.
- Add description field for Actions and Conditions.


## [1.0.0] - 2021-10-12
First release of the package. Changes made include: 
- Adding StateMachines namespace.
- Using assembly definition files
- Remonving unused or specific scripts.
- Folder reorganisation.
- Adding StateMachineActionSO.
- Adding README files for each folder.
