# GameLibrary - StateMachines

A library package/framework to build StateMachines using ScriptableObjects.

## Adding the Package

This package can easily be added via git url with `https://gitlab.com/Eyap/gamelibrary.statemachines.git`.

## License

Initial work made by [UnityTechnologies](https://github.com/UnityTechnologies), provided under the Apache License 2.0, see [COPY](./COPY) file.
You will find the list of changes in the [CHANGELOG](./CHANGELOG.md) file.

All the files in this package are released under the MPL-2.0 License, see [LICENSE](./LICENSE.md).
